import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { firebaseMutations } from 'vuexfire'
import { highscoreRef } from '../Firebase';

/* eslint-disable no-new */




Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        categories: [{
            "id": 9,
            "name": "General Knowledge"
        }, {
            "id": 10,
            "name": "Entertainment: Books"
        }, {
            "id": 11,
            "name": "Entertainment: Film"
        }, {
            "id": 12,
            "name": "Entertainment: Music"
        }, {
            "id": 13,
            "name": "Entertainment: Musicals & Theatres"
        }, {
            "id": 14,
            "name": "Entertainment: Television"
        }, {
            "id": 15,
            "name": "Entertainment: Video Games"
        }, {
            "id": 16,
            "name": "Entertainment: Board Games"
        }, {
            "id": 17,
            "name": "Science & Nature"
        }, {
            "id": 18,
            "name": "Science: Computers"
        }, {
            "id": 19,
            "name": "Science: Mathematics"
        }, {
            "id": 20,
            "name": "Mythology"
        }, {
            "id": 21,
            "name": "Sports"
        }, {
            "id": 22,
            "name": "Geography"
        }, {
            "id": 23,
            "name": "History"
        }, {
            "id": 24,
            "name": "Politics"
        }, {
            "id": 25,
            "name": "Art"
        }, {
            "id": 26,
            "name": "Celebrities"
        }, {
            "id": 27,
            "name": "Animals"
        }, {
            "id": 28,
            "name": "Vehicles"
        }, {
            "id": 29,
            "name": "Entertainment: Comics"
        }, {
            "id": 30,
            "name": "Science: Gadgets"
        }, {
            "id": 31,
            "name": "Entertainment: Japanese Anime & Manga"
        }, {
            "id": 32,
            "name": "Entertainment: Cartoon & Animations"
        }],
        score: '27',
        time: 63,
        category: '',
        question: '',
        questionData: [],
        loading: false,
        endTime: false,
        aswer: false,
        hardMode: false,
        timer: false,
        timeFlow: false,
        activeTheme: 'default',
        volume: 1,
        highscore: []},
    
    mutations: {
        sort : state => state.highscore.sort((a,b) => parseInt(a.points) < parseInt(b.points)),
        topTen: state => state.highscore.splice(10),
        updateCategory: (state, payload) => state.category = payload.category,
        updateQuestionData: (state, payload) => state.questionData = payload.questionData,
        updateQuestion: (state, payload) => state.question = payload.questionData,
        loadingChange: (state, payload) => state.loading = !state.loading,
        setEndTimeTrue: (state, payload) => state.endTime = true,
        setEndTimeFalse: (state, payload) => state.endTime = false,
        setAnswerTrue: (state, payload) => state.aswer = true,
        setAnswerFalse: (state, payload) => state.aswer = false,
        updateHighscores: (state, payload) => {
            //state.highscore = highscoreRef.child('highscore
            highscoreRef.on('child_added', function(data) {
                //data.key will be like -KPmraap79lz41FpWqLI
               state.highscore.push(data.val())
            }); 
        },
        scoreUpdate: (state, payload) => state.score = payload.points,
        timeUpdate: (state, payload) => state.time = payload.time,
        addHighScore: (state,payload) => highscoreRef.push(payload.data),
        hardModeBind: (state, payload) => state.hardMode = !state.hardMode,
        timerBind: (state, payload) => state.timer = !state.timer,  
        updateActiveTheme: (state, payload) => state.activeTheme = payload.activeTheme,
        timerFlowBind: (state, payload) => state.timeFlow = !state.timeFlow,
        volumeUpdate: (state, payload) => state.volume = payload.volume,

    },
    getters: {
      //LowestFromBests: state =>  Math.min.apply(Math, state.bests.map(function(o){return o.points;}))
      players: state => state.bests,
      category : state => state.category,
      questionData: state => state.questionData,
      score: state => state.score,
      getLow: state => state.highscore.find((item, index) => index === state.highscore.length - 1),
      getHighscore: state => state.highscore
    }

    //Do something with the data



//store.watch((state) => state.category, (oldValue, newValue) => {
   // console.log('search string is changing')
   /// console.log(oldValue)
   // console.log(newValue)
})