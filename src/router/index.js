import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Settings from '@/components/Settings'
import Game from '@/components/Game'
import Ending from '@/components/Ending'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/game',
      name: 'Game',
      component: Game
    },
    {
      path: '/ending',
      name: 'Ending',
      component: Ending
    }
    ,
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    }
  ]
})
