import { initializeApp } from 'firebase';

const app = initializeApp({
    apiKey: "AIzaSyCPxIxJ2DrASnFcKfdOrYU-3xoVo69xwvA",
    authDomain: "quiz-b25fd.firebaseapp.com",
    databaseURL: "https://quiz-b25fd.firebaseio.com",
    projectId: "quiz-b25fd",
    storageBucket: "quiz-b25fd.appspot.com",
    messagingSenderId: "977541504354"
});

export const db = app.database();
export const highscoreRef = db.ref('highscore');